import Vue from 'vue'
import VueRouter from 'vue-router'
import App from "./App";

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import './app.scss'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueRouter)

import Live from "@/components/Live";
import Processed from "@/components/Processed";

const routes = [
    { path: '/', component: Processed},
    { path: '/live/', component: Live},
    { path: '/:date', component: Processed},
    { path: '/live/:date', component: Live},
]
Vue.config.productionTip = false
const router = new VueRouter({
    routes
})

console.log(router)

const app = new Vue({
    render: h => h(App),
    router
}
)

app.$mount('#app')


